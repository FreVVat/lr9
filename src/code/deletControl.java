package code;

import java.net.URL;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class deletControl extends loginControl {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button delApplyButton;

    @FXML
    private Button backButton;

    @FXML
    private Button DBButton;

    @FXML
    private TextField deletField;

    @FXML
    void initialize() {
        delApplyButton.setOnAction(event -> {
            if(deletField.getText().equals("")){
                masseg("Error","Поля вводу не заповнені");
            }
            else {
                DBdel(deletField.getText());
            }
        });

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();
            nextscena("/scena/menu.fxml");
        });

        DBButton.setOnAction(event -> {
            DBButton.getScene().getWindow();
            sql="select * from phones";
            nextscena("/scena/outDB.fxml");
        });
    }

}
