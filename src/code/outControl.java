package code;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableView;

public class outControl extends menuControl {
    protected ObservableList<Phon> pDate = FXCollections.observableArrayList();
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button exitButton;

    @FXML
    private Button backButton;

    @FXML
    private Button reloadButton;

    @FXML
    private TableView<Phon> outTable;

    @FXML
    private TableColumn<Phon, Integer> idColumn;

    @FXML
    private TableColumn<Phon, String> devColumn;

    @FXML
    private TableColumn<Phon, String> modelColumn;

    @FXML
    private TableColumn<Phon, Double> digColumn;

    @FXML
    private TableColumn<Phon, Double> priceColumn;

    @FXML
    void initialize() {
        initData(sql); outData();

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();
            nextscena("/scena/menu.fxml");
        });

        exitButton.setOnAction(event -> {
            exitButton.getScene().getWindow().hide();
        });

        reloadButton.setOnAction(event -> {
            reloadButton.getScene().getWindow().hide();
            nextscena("/scena/outDB.fxml");
        });
    }
    private void outData() {
        idColumn.setCellValueFactory(new PropertyValueFactory<Phon, Integer>("id"));
        devColumn.setCellValueFactory(new PropertyValueFactory<Phon, String>("dev"));
        modelColumn.setCellValueFactory(new PropertyValueFactory<Phon, String>("model"));
        digColumn.setCellValueFactory(new PropertyValueFactory<Phon,Double>("dig"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<Phon,Double>("price"));
        outTable.setItems(pDate);
    }

    private void initData(String sql) {
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                pDate.add(new Phon(rs.getInt("id"), rs.getString("dev"), rs.getString("model"), rs.getDouble("dig"), rs.getDouble("price")));
            }
        } catch (Exception e) {
            System.out.println("З'єднання з БД встановити не вдалося");
            e.printStackTrace();
        }
    }

}
