package code;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class loginControl  {
    protected Stage stage = new Stage();
    protected Alert alert = new Alert(Alert.AlertType.INFORMATION);
    protected static Connection conn; static Statement st; static ResultSet rs;
    static protected String sql="";

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField loginField;

    @FXML
    private Button enterButton;

    @FXML
    private PasswordField passField;

    @FXML
    void initialize() {

        enterButton.setOnAction(event -> {
            String user = loginField.getText();//.trim();
            String pass = passField.getText();//.trim();
            if(pass.equals("") || user.equals("")){
                masseg("Error","Поля вводу не заповнені");
            }
            else {
                conn=loginUser(user,pass);
                if(conn!=null){
                    enterButton.getScene().getWindow().hide();
                    nextscena("/scena/menu.fxml");
                }
                else {
                    masseg("Error","З'єднання з БД встановити не вдалося");
                }

            }
        });
    }



    protected void nextscena(String scena){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(scena));
        try {  loader.load();  } catch (IOException e) { e.printStackTrace(); }
        Parent root = loader.getRoot();
        stage.setScene(new Scene(root));
        stage.setTitle("lr9");
        stage.show();
    }

    protected void masseg(String name,String text){
        alert.setTitle(name); alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

    private Connection loginUser(String user, String pass) {
        String url = "jdbc:mysql://127.0.0.1:3306/mydb";
        String driver = "com.mysql.jdbc.Driver";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            System.out.println("Клас драйвера не вдалося зареєструвати");
            e.printStackTrace();}
        try{
            Connection c = DriverManager.getConnection(url, user, pass);
            System.out.println("\nЗ'єднання з БД успішно встановлено");
            return c;
        } catch(Exception e){
            System.out.println("З'єднання з БД встановити не вдалося");
            e.printStackTrace();
        }
        return null;
    }

    public class Phon {
        private int id;
        private String dev;
        private String model;
        private double dig;
        private double price;

        public Phon(int id, String dev, String model, double dig, double price) {
            this.id = id;
            this.dev = dev;
            this.model = model;
            this.dig = dig;
            this.price = price;
        }

        public int getId() {
            return id;
        }

        public String getDev() {
            return dev;
        }

        public String getModel() {
            return model;
        }

        public double getDig() {
            return dig;
        }

        public double getPrice() {
            return price;
        }
    }

    public class ChoiseB{
        String name; String chBox;

        public ChoiseB(String name, String chBox) {
            this.name = name;
            this.chBox = chBox;
        }


        public String getName() {
            return name;
        }

        public String getChBox() {
            return chBox;
        }
    }

    public static void DBadd(String Dname, String mdl, double diag, double prc, Connection c){
        try{
            String sql = "INSERT INTO phones(dev,model,dig,price)"+"VALUES('"+Dname+"','"+mdl+"','"+diag+"','"+prc+"');";
            Statement statement = c.createStatement();
            int rows = statement.executeUpdate(sql);
        } catch(Exception e) {
            System.out.println("З'єднання з БД встановити не вдалося");
            e.printStackTrace();
        }
    }

    public static void DBchange(String dbid, String dbk, String dbch, Connection conn){
        try{
            String sql = "UPDATE phones SET " + dbk+" = '"+dbch+"' where id = "+dbid;
            Statement statement = conn.createStatement();
            int rows= statement.executeUpdate(sql);
        }catch(Exception e) {
            System.out.println("З'єднання з БД встановити не вдалося");
            e.printStackTrace();
        }
    }

    public static void DBdel(String id){
        try{
            String sql = "DELETE FROM phones WHERE id = "+id;
            Statement statement = conn.createStatement();
            int rows= statement.executeUpdate(sql);
        }catch(Exception e) {
            System.out.println("З'єднання з БД встановити не вдалося");
            e.printStackTrace();
        }
    }

}
