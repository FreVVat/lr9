package code;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class menuControl extends loginControl {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button outButton;

    @FXML
    private Button addButton;

    @FXML
    private Button variantButton;

    @FXML
    private Button avgButton;

    @FXML
    private Button changeButton;

    @FXML
    private Button deletButton;

    @FXML
    private Button exitButton;

    @FXML
    void initialize() {

        addButton.setOnAction(event -> {
            addButton.getScene().getWindow().hide();
            nextscena("/scena/addDB.fxml");
        });

        exitButton.setOnAction(event -> {
            try {
                if (conn != null) {
                    conn.close();
                    System.out.println("\nЗ'єднання з БД скасовано");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            exitButton.getScene().getWindow().hide();
            stage.close();
        });

        avgButton.setOnAction(event -> {
            avgButton.getScene().getWindow().hide();
            sql="select * from phones";
            nextscena("/scena/outDB.fxml");
            try {
                st = conn.createStatement();
                rs = st.executeQuery("SELECT AVG(dig) FROM phones");
                String text = "";
                if (rs.next()) text = "Середнє значення діагоналі: " + rs.getFloat(1);
                masseg("lr10", text);
            } catch (Exception e) {
                System.out.println("З'єднання з БД встановити не вдалося");
                e.printStackTrace();
            }
        });

        outButton.setOnAction(event -> {
            outButton.getScene().getWindow().hide();
            sql="select * from phones";
            nextscena("/scena/outDB.fxml");
        });

        variantButton.setOnAction(event -> {
            outButton.getScene().getWindow().hide();
            sql="select * from phones where dev ='Samsung' AND dig >6.2";
            nextscena("/scena/outDB.fxml");
            masseg("lr10","Телефони Samsung з діагональю більше 6,2");
        });

        deletButton.setOnAction(event -> {
            outButton.getScene().getWindow().hide();
            sql="select * from phones";
            nextscena("/scena/deletDB.fxml");
        });

        changeButton.setOnAction(event -> {
            changeButton.getScene().getWindow().hide();
            sql="select * from phones";
            nextscena("/scena/changeDB.fxml");
        });
    }
}
