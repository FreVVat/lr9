package code;

import java.sql.Connection;
import java.sql.Statement;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class addControl extends loginControl  {

    @FXML
    private Button addApplyButton;

    @FXML
    private Button backButton;

    @FXML
    private Button DBButton;

    @FXML
    private TextField devField;

    @FXML
    private TextField modelField;

    @FXML
    private TextField digField;

    @FXML
    private TextField priceField;


    @FXML
    void initialize() {

        DBButton.setOnAction(event -> {
            DBButton.getScene().getWindow();
            sql="select * from phones";
            nextscena("/scena/outDB.fxml");
        });

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();
            nextscena("/scena/menu.fxml");
        });

        addApplyButton.setOnAction(event -> {
            if(digField.getText().equals("") || modelField.getText().equals("") || priceField.getText().equals("") || devField.getText().equals("")){
                masseg("Error","Поля вводу не заповнені");
            }
            else {
                DBadd(devField.getText(), modelField.getText(), Double.parseDouble(digField.getText()), Double.parseDouble(priceField.getText()), conn);
            }
        });
    }
}
