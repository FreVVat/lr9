package code;

import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;

public class changeControl extends loginControl {

    ChoiseB dev = new ChoiseB("Виробние","dev");
    ChoiseB model = new ChoiseB("Модель","model");
    ChoiseB dig = new ChoiseB("Діагональ","dig");
    ChoiseB price = new ChoiseB("Ціна","price");
    ObservableList<ChoiseB> chB = FXCollections.observableArrayList(dev,model,dig,price);

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button chgApplyButton;

    @FXML
    private TextField chgField;

    @FXML
    private Button backButton;

    @FXML
    private TextField valueField;

    @FXML
    private ChoiceBox<ChoiseB> columField;

    @FXML
    private Button DBButton;

    @FXML
    void initialize() {

        columField.setConverter(new StringConverter<ChoiseB>() {
            @Override
            public String toString(ChoiseB object) {
                if(object!=null) return object.getName();
                return "";
            }

            @Override
            public ChoiseB fromString(String string) {
                return null;
            }
        });
        columField.setItems(chB);

        chgApplyButton.setOnAction(event -> {
            ChoiseB buff = new ChoiseB("",""); buff=columField.getValue();
            if(chgField.getText().equals("") || valueField.getText().equals("") || buff.getChBox().equals("")){
                masseg("Error","Поля вводу не заповнені");
            }
            else {
                DBchange(chgField.getText(), buff.getChBox(), valueField.getText(), conn);
            }
        });

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();
            nextscena("/scena/menu.fxml");
        });

        DBButton.setOnAction(event -> {
            DBButton.getScene().getWindow();
            sql="select * from phones";
            nextscena("/scena/outDB.fxml");
        });

    }


}
